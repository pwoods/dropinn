<?php 

namespace Craft;

class ShareCounterService extends  BaseApplicationComponent {

	public $apiPoints = array(
		'twitter' => array(
			'url' => 'http://cdn.api.twitter.com/1/urls/count.json?url=',
			'attr' => 'count'
		),
		'facebook' => array(
			"url" => 'http://graph.facebook.com/?id=',
			"attr" => "shares"
		),
		'pinterest' => array(
			"url" => 'http://api.pinterest.com/v1/urls/count.json?url=',
			"attr" => "count"
		),
		'linkedin' => array(
			"url" => 'http://www.linkedin.com/countserv/count/share?format=json&url=',
			"attr" => "count"
		)
	);

	public function getCount($service, $url = '') {

		if(!array_key_exists($service, $this->apiPoints)) {
			return false;
		}

		$url = $url ? $url : craft()->request->getHostInfo() . craft()->request->getUrl();
		$serviceUrl = $this->apiPoints[$service]['url'] . $url;
		$request = $this->_curlRequest($serviceUrl, $service);

		if (!empty($request)) {

			$returnedVar = $this->apiPoints[$service]['attr'];
			return isset($request->$returnedVar) ? $request->$returnedVar : 0;

		} else {
			return false;
		}

	}

	private function _curlRequest($url = '', $service = '')
	{

	$cache = craft()->fileCache->get($url);

	if ($cache) {
		return json_decode($cache);
	}

	try {
		$client   = new \Guzzle\Http\Client();
		$request  = $client->get($url, array(), array(
			'headers' => array(
				'Content-Type'    => 'application/json; charset=utf-8',
				'Accept'          => 'application/json, text/javascript, */*; q=0.01',
				'Accept-Encoding' => 'gzip'
				)
			));
		$response = $request->send();

		if ( ! $response->isSuccessful()) {
			return;
		}

		$resp = $response->getBody(true);

		if ($service == 'pinterest') {

			$resp = preg_replace('/^receiveCount\((.*)\)$/', "\\1", $resp);

		}
		craft()->fileCache->set($url, $resp, 900);

		return json_decode($resp);

	} catch(\Exception $e) {
		return;
	}
}


}