<?php
namespace Craft;
class ShareCounterVariable
{
	
	public function getCount($service, $url = '')
	{		
		return craft()->shareCounter->getCount($service, $url);
	}	
}