<?php
namespace Craft;

class ShareCounterPlugin extends BasePlugin
{

	public function getName()
	{
		return Craft::t('Share Counter');
	}

	public function getVersion()
	{
		return '0.1';
	}

	public function getDeveloper()
	{
		return 'Sean O\'Grady';
	}

	public function getDeveloperUrl()
	{
		return 'http://lovindublin.com';
	}

	public function hasCpSection() {
		return false;
	}
	public function addTwigExtension()
	{
		Craft::import('plugins.sharecounter.twigextensions.ShareCounterTwigExtension');

		return new ShareCounterTwigExtension();
	}

}
