<?php 

namespace Craft;

use Twig_Extension;
use Twig_Function_Method;

class ShareCounterTwigExtension extends \Twig_Extension {
	
	public function getName(){
		return 'Share Count';
	}

	public function getFunctions()  
	{
	    return array(
	        'shareCount' => new \Twig_Function_Method($this, 'getCount')
	    );
	}

	public function getCount($service, $url = '')
	{		
		return craft()->shareCounter->getCount($service, $url);
	}	




}
