## Synopsis

This [Craft CMS](http://buildwithcraft.com) plugin allows you to get the share count of a URL from many social media services.

## Supported Services
- Facebook - `facebook`
- Twitter - `twitter`
- LinkedIn - `linkedin`
- Pinterest - `pinterest`

### Coming Soon

- Google plus
- Set Cache Duration (currently set to every 15 minutes)
- Minor Refactoring 

## Installation

**Clone** or **download** the zip file, extract and move the folder into `craft/plugins` & rename the folder to `sharecounter`.


## Usage


If you want to get the share count for the current page, you can simply call the 
function `shareCount` with whatever social service you want to use. Example below.

`{{ shareCount('twitter') }}`

This will return an number or `0` if there is no count received.




If you want to get the share count for a different URL, you can do so by passing a URL into the optional second parameter. 

`{{ shareCount('linkedin', 'http://example.com') }}`


## Motivation

This is one my first contributions to the Craft community. While nothing special, it served one of my goals and I thought I could share it with anyone looking to do what this plugin can. 

I can offer no guarantee of prolonged support unfortunately!

## License

This licence is licensed under the [☺ Licence](http://licence.visualidiot.com/). Basically? Do whatever you want with it.