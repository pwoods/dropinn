<?php
namespace Craft;

/**
 * Redactor Table plugin
 */
class RedactorTablePlugin extends BasePlugin
{
	function getName()
	{
		return 'Redactor Table';
	}

	function getVersion()
	{
		return '1.0';
	}

	function getDeveloper()
	{
		return 'carlcs';
	}

	function getDeveloperUrl()
	{
		return 'https://github.com/carlcs/craft-redactortable';
	}

	public function init()
	{
		if (craft()->request->isCpRequest())
		{
			craft()->templates->includeJsResource('redactortable/redactortable.js');
		}
	}
}
