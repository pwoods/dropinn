<?php
/**
 * Created by JetBrains PhpStorm.
 * User: gcdtech
 * Date: 09/09/2014
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */

namespace Craft;

class BibleVerseVariable
{
	public function verse($verse) {
		return craft()->verse->getVerse($verse);
	}
}