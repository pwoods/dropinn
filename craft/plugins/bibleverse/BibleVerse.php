<?php

namespace Craft;

class BibleVersePlugin extends BasePlugin
{
	function getName()
	{
		return Craft::t('Bible Verse');
	}

	function getVersion()
	{
		return '1.0';
	}

	function getDeveloper()
	{
		return 'Pale Weasel';
	}

	function getDeveloperUrl()
	{
		return 'http://twitter.com/paleweasel';
	}
	public function getSettingsHtml()
	{
		return UrlHelper::getUrl('bibleverse/settings');
	}
}