<?php
namespace Craft;

class BibleVerseFieldType extends BaseFieldType
{
	public function getName()
	{
		return Craft::t('Bible Verse');
	}

	public function getInputHtml($name, $value)
	{
		return craft()->templates->render('bibleverse/templates/settings', array(
			'name'  => $name,
			'value' => $value
		));
	}
}