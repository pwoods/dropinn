<?php
/**
 * Created by JetBrains PhpStorm.
 * User: gcdtech
 * Date: 09/09/2014
 * Time: 12:02
 * To change this template use File | Settings | File Templates.
 */

<?php
namespace Craft;

class BibleVerseService extends BaseApplicationComponent
{
	public function getVerse($verse)
	{
		$key = "IP";
		$passage = urlencode($verse);
		$options = "include-passage-references=false";
		$url = "http://www.esvapi.org/v2/rest/passageQuery?key=$key&passage=$passage&$options";
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);
		print $response;
	}
}

