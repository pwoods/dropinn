<?php
namespace Craft;

/**
 * Events plugin class
 */
class CalendarsPlugin extends BasePlugin
{

	public function getName()
	{
		return 'Calendars';
	}

	public function getVersion()
	{
		return '0.6.4';
	}

	public function getDeveloper()
	{
		return 'Michael Rog';
	}

	public function getDeveloperUrl()
	{
		return 'http://michaelrog.com/craftcalendars';
	}

	public function hasCpSection()
	{
		return true;
	}

	public function registerCpRoutes()
	{
		return array(
			'calendars'                     => array('action' => 'calendars/calendars/calendarIndex'),
			'calendars/new'                 => array('action' => 'calendars/calendars/editCalendar'),
			'calendars/(?P<calendarId>\d+)' => array('action' => 'calendars/calendars/editCalendar'),
			'calendars/dev'                 => array('action' => 'calendars/calendars/dev'),
		);
	}

	public function onAfterInstall()
	{
		craft()->calendars_beta->phoneHome();
	}

}