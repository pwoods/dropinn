<?php
namespace Craft;

class Calendars_EventDataRecord extends BaseRecord
{
	public function getTableName(){
		return 'calendars_eventdata';
	}

	public function defineRelations(){
		return array(
			'element' => array(static::BELONGS_TO, 'ElementRecord', 'required' => true, 'onDelete' => static::CASCADE),
			'calendar' => array(static::BELONGS_TO, 'Calendars_CalendarRecord', 'required' => true, 'onDelete' => static::CASCADE),
			'field' => array(static::BELONGS_TO, 'FieldRecord'),
		);
	}

	protected function defineAttributes()
	{
		return array(
			'fieldHandle'       => AttributeType::String,
			'elementType'       => AttributeType::String,
			'startDate'         => AttributeType::DateTime,
			'endDate'           => AttributeType::DateTime,
			'allDay'            => AttributeType::Bool,
			'repeats'           => AttributeType::Bool,
			'repeatsForever'    => AttributeType::Bool,
			'lastPossibleDate'  => AttributeType::DateTime,
			'recurrenceRules'   => array(AttributeType::String, 'column' => ColumnType::Text),
			'exceptionRules'    => array(AttributeType::String, 'column' => ColumnType::Text),
			'locale'            => AttributeType::Locale,
			'timeZoneOffset'    => AttributeType::Number,
		);
	}

	/**
	 * @return array
	 */
	public function scopes()
	{
		return array(
			'ordered' => array('order' => 'startDate'),
		);
	}

}