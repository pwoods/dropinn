<?php
namespace Craft;

/**
 * Calendars controller
 */
class Calendars_CalendarsController extends BaseController
{
	/**
	 * Calendar index
	 */
	public function actionCalendarIndex()
	{
		$variables['calendars'] = craft()->calendars_calendars->getAllCalendars();

		$this->renderTemplate('calendars/calendars/index', $variables);
	}

	/**
	 * Edit a calendar.
	 *
	 * @param array $variables
	 * @throws HttpException
	 * @throws Exception
	 */
	public function actionEditCalendar(array $variables = array())
	{
		$variables['brandNewCalendar'] = false;

		if (!empty($variables['calendarId']))
		{
			if (empty($variables['calendar']))
			{
				$variables['calendar'] = craft()->calendars_calendars->getCalendarById($variables['calendarId']);

				if (!$variables['calendar'])
				{
					throw new HttpException(404);
				}
			}

			$variables['title'] = $variables['calendar']->name;
		}
		else
		{
			if (empty($variables['calendar']))
			{
				$variables['calendar'] = new Calendars_CalendarModel();
				$variables['brandNewCalendar'] = true;
			}

			$variables['title'] = Craft::t('Create a new calendar');
		}

		$variables['crumbs'] = array(
			array('label' => Craft::t('Calendars'), 'url' => UrlHelper::getUrl('calendars'))
		);

		$this->renderTemplate('calendars/calendars/_edit', $variables);
	}

	/**
	 * Saves a calendar
	 */
	public function actionSaveCalendar()
	{
		$this->requirePostRequest();

		$calendar = new Calendars_CalendarModel();

		// Shared attributes
		$calendar->id         = craft()->request->getPost('calendarId');
		$calendar->name       = craft()->request->getPost('name');
		$calendar->handle     = craft()->request->getPost('handle');

		// Set the field layout
		$fieldLayout = craft()->fields->assembleLayoutFromPost();
		$fieldLayout->type = ElementType::Asset;
		$calendar->setFieldLayout($fieldLayout);

		// Save it
		if (craft()->calendars_calendars->saveCalendar($calendar))
		{
			craft()->userSession->setNotice(Craft::t('Calendar saved.'));
			$this->redirectToPostedUrl($calendar);
		}
		else
		{
			craft()->userSession->setError(Craft::t('Couldn’t save calendar.'));
		}

		// Send the calendar back to the template
		craft()->urlManager->setRouteVariables(array(
			'calendar' => $calendar
		));
	}

	/**
	 * Deletes a calendar.
	 */
	public function actionDeleteCalendar()
	{
		$this->requirePostRequest();
		$this->requireAjaxRequest();

		$calendarId = craft()->request->getRequiredPost('id');

		craft()->calendars_calendars->deleteCalendarById($calendarId);
		$this->returnJson(array('success' => true));
	}

	/**
	 * @deprecated For internal purposes only.
	 */
	public function actionDev()
	{
		$this->renderTemplate('calendars/dev/field', array());
	}
}
