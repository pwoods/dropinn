<?php
namespace Craft;

class Calendars_EventDataFieldType extends BaseFieldType
{

	// TODO: Document all the things.

	private $_inputData;

	public function getName()
	{
		return Craft::t('Event Data');
	}

	public function defineContentAttribute()
	{
		return AttributeType::DateTime;
	}

	/**
	 * getInputHtml
	 *
	 * @param string $name Our fieldtype handle
	 * @param string $value Our fieldtype value from the content table
	 * @return string Return the field's input template
	 */
	public function getInputHtml($name, $value = null)
	{

		$id = craft()->templates->formatInputId($name);
		$namespaceId = craft()->templates->namespaceInputId($id);

		craft()->templates->includeJsResource('calendars/js/ko.js');
		craft()->templates->includeJsResource('calendars/js/eventDataField.js');

		if (!isset($this->_inputData))
		{

			$eventDataRecord = Calendars_EventDataRecord::model()->findByAttributes(array(
				'elementId' => $this->element->id,
				'calendarId' => $this->getSettings()->calendarId,
				'fieldId' => $this->model->id,
				// TODO: Fix criteria for when getInputHtml is called from a non-field context, like on an Event element
			));

			$this->_inputData = Calendars_EventDataModel::populateModel($eventDataRecord);

		}

		return craft()->templates->render('calendars/eventData/_field', array(
			'name'  => $name,
			'id' => craft()->templates->formatInputId($name),
			'namespaceId' => $namespaceId,
			'model' => $this->model,
			'element' => $this->element,
			'inputData' => $this->_inputData,
		));

	}

	protected function defineSettings()
	{
		return array(
			'calendarId' => array(AttributeType::Number, 'min' => 1)
		);
	}

	public function getSettingsHtml()
	{
		return craft()->templates->render('calendars/eventData/_fieldSettings', array(
			'settings' => $this->getSettings()
		));
	}

	public function prepSettings($settings)
	{
		// Modify $settings here...
		return $settings;
	}

	// Fetch eventData and prepare it for use in the template or getInputHtml()
	public function prepValue($value)
	{
		// Modify $value here...
		return $value;
	}

	// Validate eventData from submission
	public function validate($value){
		return $this->_validateEventData($this);
	}

	// Fetch eventData and prepare it for use in the template.
	public function prepValueFromPost($value)
	{
		// We store the original event's startDate in the craft_content table
		return $value['startDate'];
	}

	// Save the eventDate in its own table.
	public function onAfterElementSave()
	{
		// After saving element, save field to plugin table
		// Returns true if entry was saved
		return $this->_saveEventData($this);
	}



	// -------- Methods for validating/saving eventData -------- //

	private function _validateEventData(BaseFieldType $fieldType)
	{

		$postContent = $fieldType->element->getContentFromPost();
		$value = $postContent[$fieldType->model->handle];

		$recurrenceRulesDecoded = json_decode($value['recurrenceRules']);

		// If startDate, endDate, and recurrenceRules are all empty, this isn't meant to be an event.
		// (This is a valid case, and saveEventData() will take care of it.)
		if
		(
			empty($value['startDate']['date']) && empty($value['startDate']['time'])
			&& empty($value['endDate']['date']) && empty($value['endDate']['time'])
			&& empty($recurrenceRulesDecoded->rules)
		)
		{
			return true;
		}

		// Okay, so we have a field with eventData in it...

		// Take care of a few cases for Lazy Content Editors
		$value = $this->_cleanUpInputData($value);

		// Set up a model on which we can attach errors and manipulate values...
		$this->_inputData = Calendars_EventDataModel::populateModel($value);

		// Now try to validate the data...

		$fieldErrors = false;

		// Validate the startDate

		if (empty($value['startDate']['date'])) {
			$fieldErrors = true;
			$this->_inputData->addError('startDate', "You must specify a starting date/time.");
		}

		// Validate the endDate

		if (empty($value['endDate']['date']))
		{
			$fieldErrors = true;
			$this->_inputData->addError('endDate', "You must specify an ending date/time. ");
		}

		if ( DateTime::createFromString($value['endDate']) < DateTime::createFromString($value['startDate']) )
		{
			$fieldErrors = true;
			$this->_inputData->addError('endDate', "The ending date must be later than the starting date. ");
		}

		// Validate the recurrenceRules
		// $validatedRules = $this->_validateRules($recurrenceRulesDecoded->rules);

		if($fieldErrors){
			return array("(Please correct the errors listed above.)");
		}
		else{
			return true;
		}

	}

	private function _saveEventData(BaseFieldType $fieldType)
	{

		$postContent = $fieldType->element->getContentFromPost();
		$value = $postContent[$fieldType->model->handle];

		$elementId = $fieldType->element->id;
		$calendarId = $fieldType->getSettings()->calendarId;
		$fieldId = $fieldType->model->id;
		// TODO (2.0): Fix this for when we're not in a field context, like with an Events element.

		// Attempt to load existing Record

		$eventDataRecord = Calendars_EventDataRecord::model()->findByAttributes(array(
			'elementId' => $elementId,
			'calendarId' => $calendarId,
			'fieldId' => $fieldId,
		));

		// If startDate, endDate, and recurrenceRules are all empty, this isn't meant to be an event.
		// We should either delete the existing EventDataRecord, or never create one, and move on.

		$recurrenceRulesDecoded = json_decode($value['recurrenceRules']);

		if
		(
			empty($value['startDate']['date']) && empty($value['startDate']['time'])
			&& empty($value['endDate']['date']) && empty($value['endDate']['time'])
			&& empty($recurrenceRulesDecoded->rules)
		)
		{
			if ($eventDataRecord)
			{
				// Delete this Record.
				$eventDataRecord->delete();
				return true;
			}
			else
			{
				// Nothing to save.
				return false;
			}
		}

		// Okay, we apparently have a field filled with enough eventData to be valid...

		// Take care of a few cases for Lazy Content Editors
		$value = $this->_cleanUpInputData($value);

		// If no Record exists, create new Record
		if (!$eventDataRecord)
		{
			$eventDataRecord = new Calendars_EventDataRecord();
			$eventDataRecord->setAttributes(
				array(
					'elementId' => $elementId,
					'calendarId' => $calendarId,
					'fieldId' => $fieldId,
				), false
			);
		}

		$startDateTime = DateTime::createFromString($value['startDate'], craft()->getTimeZone());
		$endDateTime = DateTime::createFromString($value['endDate'], craft()->getTimeZone());
		$isAllDay = ($value['startDate']['time'] == "12:00 AM" && ($value['endDate']['time'] == "12:00 AM" || $value['endDate']['time'] == "11:59 PM"));

		// Set the full list of attributes on the Record
		$attr = array(
			'fieldHandle' => $fieldType->model->handle,
			'elementType' => $fieldType->element->elementType,
			'startDate' => $startDateTime,
			'endDate' => $endDateTime,
			'allDay' => $isAllDay,
			'repeats' => null,
			'repeatsForever' => null,
			'lastPossibleDate' => null,
			'recurrenceRules' => $value['recurrenceRules'],
			'exceptionRules' => null,
			'locale' => $fieldType->element->locale,
			'timeZoneOffset' => craft()->calendars_eventData->DTZ()->getOffset($startDateTime),
		);
		$eventDataRecord->setAttributes($attr, false);

		// ...and save the new Record
		$eventDataSaved = $eventDataRecord->save();
		$id = $eventDataRecord->id;

		return $eventDataSaved;

	}

	private function _cleanUpInputData($value)
	{

		// Take care of a few cases for Lazy Content Editors

		if (empty($value['startDate']['time'])) {
			$value['startDate']['time'] = "12:00 AM";
		}

		if (empty($value['endDate']['time'])) {
			$value['endDate']['time'] = "11:59 PM";
		}

		return $value;

	}


	private function _validateRules(Array $rules)
	{

		$errors = 0;

		foreach($rules as $r) {
			$t = $this;
		}

		return array
		(
			'validatedRules' => $rules,
			'errors' => $errors
		);

	}

	private function _validateRule($rule)
	{

	}

	private function _cleanUpRule($rule)
	{

	}


}