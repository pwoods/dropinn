<?php
namespace Craft;

/**
 * Craft Calendars EventDataCriteriaModel
 */
class Calendars_EventDataCriteriaModel extends BaseModel
{


	// TODO: document variables

	/**
	 * @var
	 */
	private $_fetchedEvents;

	/**
	 * @var
	 */
	private $_fetchedEventsTotal;

	/**
	 * @var
	 */
	private $_sortedEvents;

	/**
	 * @var
	 */
	private $_sortedEventsCount;

	/**
	 * @var
	 */
	private $_fetchedDates;

	/**
	 * @var
	 */
	private $_fetchedDatesTotal;

	/**
	 * @var
	 */
	private $_sortedDates;

	/**
	 * @var
	 */
	private $_sortedDatesCount;

	/**
	 * @var
	 */
	private $_fetchedElements;

	/**
	 * @var
	 */
	private $_fetchedElementIds;

	/**
	 * @var
	 */
	private $_fetchedDebugData;

	/**
	 * @var
	 */
	private $_eventsByDate;




	/**
	 * String representation for the model
	 *
	 * @return string
	 */
	function __toString()
	{
		return Craft::t("Craft Calendars EventDataCriteriaModel");
	}

	/**
	 * @inheritDoc BaseModel::defineAttributes()
	 * @access protected
	 * @return array
	 */
	protected function defineAttributes()
	{

		$attributes = array(

			'dateRangeStart'    => AttributeType::Mixed,
			'dateRangeEnd'      => AttributeType::Mixed,
			'startsAfter'       => AttributeType::Mixed,
			'startsBefore'      => AttributeType::Mixed,
			'endsBefore'        => AttributeType::Mixed,
			'endsAfter'         => AttributeType::Mixed,

			'calendarId'        => AttributeType::Number,
			'elementId'         => AttributeType::Number,
			'fieldId'           => AttributeType::Number,

			'fieldHandle'       => AttributeType::String,
			'elementType'       => AttributeType::String,
			'startDate'         => AttributeType::Mixed,
			'endDate'           => AttributeType::Mixed,
			'repeats'           => AttributeType::Bool,
			'repeatsForever'    => AttributeType::Bool,
			'lastPossibleDate'  => AttributeType::Mixed,
			'locale'            => AttributeType::Locale,
			'dateCreated'       => AttributeType::Mixed,
			'dateUpdated'       => AttributeType::Mixed,

			'excludeOriginalOccurrences' => array(AttributeType::Bool, 'default' => false),
			'excludeRecurrences' => array(AttributeType::Bool, 'default' => false),

			'eventLimit'        => array(AttributeType::Number),
			'eventOffset'       => array(AttributeType::Number, 'default' => 0),
			'eventOrder'        => array(AttributeType::String, 'default' => 'startDate asc'),

			'dateLimit'         => array(AttributeType::Number),
			'dateOffset'        => array(AttributeType::Number, 'default' => 0),
			'dateOrder'         => array(AttributeType::String, 'default' => 'asc'),

			'fetchElements'     => array(AttributeType::Bool, 'default' => true),

			// TODO: Implement Element-based search
			'search'            => AttributeType::String,

		);

		return $attributes;

	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return array();
	}




	/**
	 * Fetches eventData for the current criteria and repopulates the model with the new data
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 */
	private function _fetchEventData($attributes = null)
	{

		$this->setAttributes($attributes);

		// Fetch new event data
		$eventData = craft()->calendars_eventData->getEventDataArray($this);

		// Cache the fetched data
		$this->setFetchedEvents($eventData['events']);
		$this->setFetchedDates($eventData['dates']);
		$this->setFetchedElements($eventData['elements']);
		$this->_fetchedElementIds = $eventData['elementIds'];
		$this->_fetchedDebugData = $eventData['debugData'];

		// Register our events' Elements with the TemplateCacheService
		$this->_includeElementsInTemplateCaches($eventData['elementIds']);

	}

	/**
	 * Sets an attribute's value.
	 *
	 * In addition, will clears the appropriate cached values when a new attribute is set.
	 *
	 * @param string $name
	 * @param mixed $value
	 *
	 * @return bool
	 */
	public function setAttribute($name, $value)
	{

		// If this is an attribute, and the value is not actually changing,
		// just return true so the fetched data don't get cleared.
		if (in_array($name, $this->attributeNames()) && $this->getAttribute($name) === $value)
		{
			return true;
		}

		// If this isn't an attribute, return false, because there is no attribute to set.
		if (!in_array($name, $this->attributeNames()))
		{
			return false;
		}

		if (parent::setAttribute($name, $value))
		{

			$sortedEventsAttributes = array('eventLimit', 'eventOffset', 'eventOrder');
			$sortedDatesAttributes = array('dateLimit', 'dateOffset', 'dateOrder');

			// If the attribute affects only the sorted event list, we need to clear out
			// our sorted events caches, but we won't need to fetch new eventData
			// or process new sorted dates
			if (in_array($name, $sortedEventsAttributes))
			{
				$this->_clearSortedEventsData();
				return true;
			}

			// If the attribute affects only the sorted date list, we need to clear out
			// our sorted dates caches, but we won't need to fetch new eventData
			// or process new sorted events
			if (in_array($name, $sortedDatesAttributes))
			{
				$this->_clearSortedDatesData();
				return true;
			}

			// Otherwise, the attribute may invalidate our fetched data,
			// so we need to clear out all our cached data
			$this->_clearAllCachedData();
			return true;

		}
		else
		{
			return false;
		}

	}

	/**
	 * Clears out the cached sorted events data
	 */
	private function _clearSortedEventsData()
	{
		$this->_sortedEvents = null;
		$this->_sortedEventsCount = null;
		$this->_eventsByDate = null;
	}

	/**
	 * Clears out the cached sorted dates data
	 */
	private function _clearSortedDatesData()
	{
		$this->_sortedDates = null;
		$this->_sortedDatesCount = null;
		$this->_eventsByDate = null;
	}

	/**
	 * Clears out all the cached data
	 */
	private function _clearAllCachedData()
	{
		$this->_clearSortedEventsData();
		$this->_clearSortedDatesData();
		$this->_fetchedEvents = null;
		$this->_fetchedEventsTotal = null;
		$this->_fetchedDates = null;
		$this->_fetchedDatesTotal = null;
		$this->_fetchedElements = null;
		$this->_fetchedElementIds = null;
		$this->_fetchedDebugData = null;
	}




	/**
	 * Returns an array of events, filtered by the criteria
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 *
	 * @return array
	 */
	public function events($attributes = null)
	{
		$this->setAttributes($attributes);
		return $this->getSortedEvents();
	}

	/**
	 * Returns the count of sorted events
	 * (taking into consideration the eventOffset and eventLimit attributes)
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 *
	 * @return int
	 */
	public function countEvents($attributes = null)
	{

		$this->setAttributes($attributes);

		if ( !isset($this->_sortedEventsCount) )
		{
			$this->_sortedEventsCount = count($this->getSortedEvents());
		}
		return $this->_sortedEventsCount;

	}

	/**
	 * Returns the total of the fetched events
	 * (which does not take the eventOffset and eventLimit attributes into consideration)
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 *
	 * @return int
	 */
	public function totalEvents($attributes = null)
	{

		$this->setAttributes($attributes);

		if ( !isset($this->_fetchedEventsTotal) )
		{
			$this->_fetchedEventsTotal = count($this->getFetchedEvents());
		}
		return $this->_fetchedEventsTotal;

	}

	/**
	 * An alias to events(), a la ElementCriteriaModel
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 *
	 * @return array
	 */
	public function find($attributes = null)
	{
		return $this->events($attributes);
	}

	/**
	 * An alias to countEvents(), a la ElementCriteriaModel
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 *
	 * @return int
	 */
	public function count($attributes = null)
	{
		return $this->countEvents($attributes);
	}

	/**
	 * An alias to totalEvents(), a la ElementCriteriaModel
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 *
	 * @return int
	 */
	public function total($attributes = null)
	{
		return $this->totalEvents($attributes);
	}




	/**
	 * Returns an array of dates with their events, all filtered by the criteria
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 *
	 * @return array
	 */
	public function dates($attributes = null)
	{
		$this->setAttributes($attributes);
		return $this->getEventsByDate();
	}

	/**
	 * Returns the count of sorted dates in the date range
	 * (taking into consideration the dateOffset and dateLimit attributes)
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 *
	 * @return int
	 */
	public function countDates($attributes = null)
	{

		$this->setAttributes($attributes);

		if ( !isset($this->_sortedDatesCount) )
		{
			$this->_sortedDatesCount = count($this->getSortedDates());
		}
		return $this->_sortedDatesCount;

	}

	/**
	 * Returns the total of the fetched dates
	 * (which does not take the dateOffset and dateLimit attributes into consideration)
	 *
	 * @param array $attributes Any last-minute parameters that should be added.
	 *
	 * @return int
	 */
	public function totalDates($attributes = null)
	{

		$this->setAttributes($attributes);

		if ( !isset($this->_fetchedDatesTotal) )
		{
			$this->_fetchedDatesTotal = count($this->getFetchedDates());
		}
		return $this->_fetchedDatesTotal;

	}




	/**
	 * Sets the fetched event cache
	 *
	 * @param array
	 */
	public function setFetchedEvents(array $events)
	{
		$this->_fetchedEvents = $events;
	}

	/**
	 * Sets the fetched dates cache
	 *
	 * @param array
	 */
	public function setFetchedDates(array $dates)
	{
		$this->_fetchedDates = $dates;
	}

	/**
	 * Sets the fetched Elements cache
	 *
	 * @param array
	 */
	public function setFetchedElements(array $elements)
	{
		$this->_fetchedElements = $elements;
	}

	/**
	 * Gets the fetched event cache
	 *
	 * If the fetched event cache is empty, we trigger _fetchEventData to get new data.
	 *
	 * @return array
	 */
	public function getFetchedEvents()
	{

		if ( !isset($this->_fetchedEvents) )
		{
			$this->_fetchEventData();
		}
		return $this->_fetchedEvents;

	}

	/**
	 * Gets the fetched date cache
	 *
	 * If the fetched date cache is empty, we trigger _fetchEventData to get new data.
	 *
	 * @return array
	 */
	public function getFetchedDates()
	{

		if ( !isset($this->_fetchedDates) )
		{
			$this->_fetchEventData();
		}
		return $this->_fetchedDates;

	}

	/**
	 * Gets the fetched Element cache
	 *
	 * If the fetched Element cache is empty, we trigger _fetchEventData to get new data.
	 *
	 * @return array
	 */
	public function getFetchedElements()
	{

		if ( !isset($this->_fetchedElements) )
		{
			$this->_fetchEventData();
		}
		return $this->_fetchedElements;

	}

	/**
	 * An alias to getFetchedElements
	 *
	 * @return array
	 */
	public function getElements()
	{
		return $this->getFetchedElements();
	}

	/**
	 * Gets the fetched Element IDs cache
	 *
	 * If the fetched Element IDs cache is empty, we trigger _fetchEventData to get new data.
	 *
	 * @return array
	 */
	public function getFetchedElementIds()
	{

		if ( !isset($this->_fetchedElementIds) )
		{
			$this->_fetchEventData();
		}
		return $this->_fetchedElementIds;

	}

	/**
	 * An alias to getFetchedElementIds
	 *
	 * @return array
	 */
	public function getElementIds()
	{
		return $this->getFetchedElementIds();
	}

	/**
	 * Gets the fetched debug data
	 *
	 * If the fetched Debug Data cache is empty, we trigger _fetchEventData to get new data.
	 *
	 * @return array
	 */
	public function getFetchedDebugData()
	{

		if ( !isset($this->_fetchedDebugData) )
		{
			$this->_fetchEventData();
		}
		return $this->_fetchedDebugData;

	}

	/**
	 * An alias to getFetchedDebugData
	 *
	 * @return array
	 */
	public function debug()
	{
		return $this->getFetchedDebugData();
	}




	/**
	 * Gets the [cached] sorted events list
	 *
	 * If the sorted events cache is still null, we create it.
	 *
	 * @return array
	 */
	public function getSortedEvents()
	{

		if ( !isset($this->_sortedEvents) )
		{
			$this->_sortedEvents = $this->_prepSortedEvents();
		}
		return $this->_sortedEvents;

	}

	/**
	 * Gets the [cached] sorted dates list
	 *
	 * If the sorted dates cache is still null, we create it.
	 *
	 * @return array
	 */
	public function getSortedDates()
	{

		if ( !isset($this->_sortedDates) )
		{
			$this->_sortedDates = $this->_prepSortedDates();
		}
		return $this->_sortedDates;

	}

	/**
	 * Gets the [cached] ordered array of events by date
	 *
	 * If the eventsByDate cache is still null, we create it.
	 *
	 * @return array
	 */
	public function getEventsByDate()
	{

		if ( !isset($this->_eventsByDate) )
		{
			$this->_eventsByDate = $this->_prepEventsByDate();
		}
		return $this->_eventsByDate;

	}




	/**
	 * Prepares the ordered list of events according to the eventOffset, eventLimit, and eventOrder params
	 *
	 * @return array
	 */
	private function _prepSortedEvents()
	{

		$events = $this->getFetchedEvents();

		// If fetchElements param is true, attach the fetched Elements to their events
		if ($this->getAttribute('fetchElements'))
		{
			foreach ($events as $e)
			{
				$id = $e->element->id;
				if (isset($this->_fetchedElements[$id]))
				{
					$e->setAttribute('element', $this->_fetchedElements[$id]);
				}
			}
		}

		// TODO: Sort the events list
		// $events = uasort($events, null);

		// Apply offset and limit to sorted list
		$offset = $this->getAttribute('eventOffset');
		$limit = $this->getAttribute('eventLimit');
		$events = array_slice($events, $offset, $limit);

		return $events;

	}

	/**
	 * Prepares the ordered list of dates according to the dateOffset, dateLimit, and dateOrder params
	 *
	 * @return array
	 */
	private function _prepSortedDates()
	{

		$dates = $this->getFetchedDates();

		// Sort the dates
		ksort($dates);

		// Apply sort order
		if ($this->getAttribute('dateOrder') == 'desc')
		{
			$dates = array_reverse($dates, true);
		}

		// Apply offset and limit to sorted list
		$offset = $this->getAttribute('dateOffset');
		$limit = $this->getAttribute('dateLimit');
		$dates = array_slice($dates, $offset, $limit);

		return $dates;

	}

	/**
	 * Assembles the ordered array of events by date
	 *
	 * @return array
	 */
	private function _prepEventsByDate()
	{

		$events = $this->getSortedEvents();
		$dates = $this->getSortedDates();

		// TODO: Properly assign the sortedEvents to the sortedDates

		return $dates;

	}




	/**
	 * Includes the fetched events' Elements in any active template caches
	 *
	 * @param array
	 */
	private function _includeElementsInTemplateCaches($elementIds = array())
	{

		$cacheService = craft()->getComponent('templateCache', false);

		if ($cacheService)
		{
			foreach ($elementIds as $id)
			{
				$cacheService->includeElementInTemplateCaches($id);
			}
		}

	}

	/**
	 * TODO: docs
	 */
	private function _compareEvents(Calendars_EventDataModel $a, Calendars_EventDataModel $b, $sort = array()) {

		// If there aren't any dimensions to sort by, assume the objects are the same.
		if (isEmpty($sort)) { return 0; }

		// Pop the first dimension/order off the comparators array

		$comp = array_shift($sort);
		$dimension = ( isset($comp['dimension']) ? $comp['dimension'] : 'startDate' );
		$order = ( isset($comp['order']) && $comp['order'] == 'desc' ? 'desc' : 'asc' );

		// Grab the values to compare

		switch($dimension)
		{
			case 'startDate':
				$aVal = $a->startDate;
				$bVal = $b->startDate;
				break;
			case 'endDate';
				$aVal = $a->startDate;
				$bVal = $b->startDate;
				break;
			case 'allDay';
				$aVal = $a->allDay;
				$bVal = $b->allDay;
				break;
			case 'title';
				$aVal = (isset($a->element->title) ? $a->element->title : '');
				$bVal = (isset($b->element->title) ? $b->element->title : '');
				break;
			case 'dateCreated';
				$aVal = $a->dateCreated;
				$bVal = $b->dateCreated;
				break;
			case 'dateUpdated';
				$aVal = $a->dateUpdated;
				$bVal = $b->dateUpdated;
				break;
		}

		// If the dimension values are the same, recurse to the next dimension

		if ($aVal == $bVal) {
			return 0;
		}

		// Return the sort verdict
		return ($order == 'desc' ? -1 : 1) * ($aVal < $bVal ? -1 : 1);

	}


}