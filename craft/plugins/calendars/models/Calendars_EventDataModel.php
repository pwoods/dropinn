<?php
namespace Craft;

/**
 * Craft Calendars EventDataModel
 */
class Calendars_EventDataModel extends BaseModel
{
	/**
	 * Use the translated calendar name as the string representation.
	 *
	 * @return string
	 */
	function __toString()
	{
		return Craft::t('Craft Calendars EventData [{id}]', array(
			'id' => $this->id,
		));
	}

	/**
	 * @access protected
	 * @return array
	 */
	protected function defineAttributes()
	{
		return array(
			'id'                => AttributeType::Number,
			'calendar'          => AttributeType::Mixed,
			'field'             => AttributeType::Mixed,
			'fieldHandle'       => AttributeType::String,
			'element'           => AttributeType::Mixed,
			'elementType'       => AttributeType::String,
			'startDate'         => AttributeType::DateTime,
			'endDate'           => AttributeType::DateTime,
			'allDay'            => AttributeType::Bool,
			'repeats'           => AttributeType::Bool,
			'repeatsForever'    => AttributeType::Bool,
			'lastPossibleDate'  => AttributeType::DateTime,
			'recurrenceRules'   => array(AttributeType::String, 'column' => ColumnType::Text),
			'exceptionRules'    => array(AttributeType::String, 'column' => ColumnType::Text),
			'locale'            => AttributeType::Locale,
			'timeZoneOffset'    => AttributeType::Number,
			'isOriginalOccurrence' => AttributeType::Bool,
			'isRecurrence'      => AttributeType::Bool,
		);
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return array();
	}

}

