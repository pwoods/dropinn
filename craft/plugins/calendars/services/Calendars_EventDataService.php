<?php
namespace Craft;

class Calendars_EventDataService extends BaseApplicationComponent
{

	// TODO: Document variables

	private $_systemDateTimeZone;

	private $_allEventDataIds;
	private $_eventDataById;
	private $_fetchedAllEventData = false;

	private $_events = array();
	private $_dates = array();
	private $_elementsToFetch = array();
	private $_elementIds = array();
	private $_elements = array();

	/**
	 * Weekdays and their ISO numeric representation
	 *
	 * @access     private
	 * @var        array
	 */
	private $_weekdays = array(
		1 => 'monday',
		2 => 'tuesday',
		3 => 'wednesday',
		4 => 'thursday',
		5 => 'friday',
		6 => 'saturday',
		7 => 'sunday'
	);




	// -------- Methods for fetching eventData -------- //

	/**
	 * Returns all of the EventData IDs.
	 *
	 * @return array
	 */
	public function getAllEventDataIds()
	{

		if (!isset($this->_allEventDataIds))
		{
			if ($this->_fetchedAllEventData)
			{
				$this->_allEventDataIds = array_keys($this->_eventDataById);
			}
			else
			{
				$this->_allEventDataIds = craft()->db->createCommand()
					->select('id')
					->from('calendars_eventdata')
					->queryColumn();
			}
		}

		return $this->_allEventDataIds;
	}

	/**
	 * Returns all EventData.
	 *
	 * @param string|null $indexBy
	 * @return array
	 */
	public function getAllEventData($indexBy = null)
	{
		if (!$this->_fetchedAllEventData)
		{
			$eventDataRecords = Calendars_EventDataRecord::model()->ordered()->with('calendar','element','field')->findAll();
			$this->_eventDataById = Calendars_EventDataModel::populateModels($eventDataRecords, 'id');
			$this->_fetchedAllEventData = true;
		}

		if ($indexBy == 'id')
		{
			return $this->_eventDataById;
		}
		else if (!$indexBy)
		{
			return array_values($this->_eventDataById);
		}
		else
		{
			$eventData = array();

			foreach ($this->_eventDataById as $data)
			{
				$eventData[$data->$indexBy] = $data;
			}

			return $eventData;
		}
	}

	/**
	 * Gets eventData by its attributes
	 *
	 * @param array $criteria
	 * @return array
	 */
	public function getEventDataByCriteria($criteria)
	{
		$eventDataRecords = Calendars_EventDataRecord::model()->with('calendar','element','field')->findAllByAttributes($criteria);

		if ($eventDataRecords)
		{
			return Calendars_EventDataModel::populateModels($eventDataRecords);
		}
	}

	/**
	 * Gets the total number of EventData records.
	 *
	 * @return int
	 */
	public function getEventDataCount()
	{
		return count($this->getAllEventData());
	}

	/**
	 * Returns an EventData record by its ID.
	 *
	 * @param $eventDataId
	 * @return Calendars_EventDataModel|null
	 */
	public function getEventDataById($eventDataId)
	{
		if (!isset($this->_eventDataById) || !array_key_exists($eventDataId, $this->_eventDataById))
		{
			$eventDataRecord = Calendars_CalendarRecord::model()->with('calendar','element','field')->findById($eventDataId);

			if ($eventDataRecord)
			{
				$this->_eventDataById[$eventDataId] = Calendars_EventDataModel::populateModel($eventDataRecord);
			}
			else
			{
				$this->_eventDataById[$eventDataId] = null;
			}
		}

		return $this->_eventDataById[$eventDataId];
	}




	// -------- Methods for fetching data for templates -------- //

	/**
	 * Returns an EventDataCriteriaModel based on the supplied attributes
	 *
	 * @param array $attributes
	 *
	 * @return Calendars_EventDataCriteriaModel
	 */
	public function getEventDataCriteriaModel($attributes = null)
	{
		return new Calendars_EventDataCriteriaModel($attributes);
	}

	/**
	 * Assembles and returns an event data array
	 *
	 * @param Calendars_EventDataCriteriaModel $criteria
	 *
	 * @return array An event data array (with keys: events, dates, elements, elementIds, debugData)
	 */
	public function getEventDataArray($criteria)
	{

		$attributes = $criteria->getAttributes();

		$r = array(
			'events' => array(),
			'dates' => array(),
			'elements' => array(),
			'elementIds' => array(),
			'debugData' => array(),
		);

		$old = $this->getOccurrences($attributes);
		$r['events'] = $old->events;
		$r['dates'] = $old->dates;
		$r['elements'] = $this->_elements;
		$r['elementIds'] = $this->_elementIds;
		$r['debugData'] = $old->debug;
		$r['debugData']['elementsToFetch'] = $this->_elementsToFetch;

		return $r;

	}




	// -------- Methods for processing and assembling eventData into a calendar -------- //

	public function getCalendarRange($start, $end)
	{
		$start = $this->DT($start);
		$end = $this->DT($end);
		return $this->_makeCalendarRange($start, $end, true);
	}

	private function _makeCalendarRange($start, $end, $daysonly = true)
	{

		$r = array();

		$calendarRangeBegin = (clone $start);
		$calendarRangeBegin->setTime(0, 0);
		$r['beginDateTime'] = $calendarRangeBegin;

		$calendarRangeEnd = (clone $end);
		$calendarRangeEnd->modify('+1 day')->setTime(0, 0);
		$r['endDateTime'] = $calendarRangeEnd;

		$interval = new DateInterval('P1D');

		$calendarDatePeriod = new \DatePeriod($calendarRangeBegin, $interval, $calendarRangeEnd);
		$r['datePeriod'] = $calendarDatePeriod;

		$calendarDates = array();
		foreach($calendarDatePeriod as $date)
		{
			$calendarDates[] = $this->DT($date);
		}
		$r['calendarDates'] = $calendarDates;

		return ($daysonly ? $r['calendarDates'] : $r);

	}

	public function getOccurrences($params = array())
	{

		// TODO: I should create a model to pass the assembled data around in. For now it's just a stdClass.

		// Set up an object to return
		$r = (object) array(
			'now' => array(),
			'params' => array(),
			'calendar' => array(),
			'eventData' => array(),
			'events' => array(),
			'dates' => array(),
			'elements' => array(),
			'debug' => array(),
		);

		// I want to keep track of how long this takes...

		$timerStartProcessing = microtime();

		// Now -- for reference/debugging

		$nowDateTime = new DateTime("now", $this->DTZ());
		$r->now['nowTimestamp'] = $nowDateTime->getTimestamp();
		$r->now['nowOffset'] = $nowDateTime->getOffset();
		$r->now['nowDateTime'] = $nowDateTime;

		// And then create DateTime objects for our params (dateRangeStart, dateRangeEnd, startsAfter, startsBefore, endsAfter, endsBefore)

		if (!isset($params['dateRangeStart']))
		{
			$params['dateRangeStart'] = "today";
		}
		$params['dateRangeStartDateTime'] = $this->DT($params['dateRangeStart']);

		if (!isset($params['dateRangeEnd']))
		{
			$params['dateRangeEnd'] = "tomorrow";
		}
		$params['dateRangeEndDateTime'] = $this->DT($params['dateRangeEnd']);

		if (!isset($params['startsAfter']))
		{
			$params['startsAfter'] = "-1 year";
		}
		$params['startsAfterDateTime'] = $this->DT($params['startsAfter']);

		if (!isset($params['startsBefore']))
		{
			$params['startsBefore'] = "+1 years";
		}
		$params['startsBeforeDateTime'] = $this->DT($params['startsBefore']);

		if (isset($params['endsAfter']))
		{
			$params['endsAfterDateTime'] = $this->DT($params['endsAfter']);
		}

		if (isset($params['endsBefore']))
		{
			$params['endsBeforeDateTime'] = $this->DT($params['endsBefore']);
		}

		// And stick the params into our return

		$r->params = $params;

		// Now we'll calculate the range of calendar dates for which we should check occurrences/recurrences
		// and form the dates object we'll return to the Variable

		$r->calendar = $this->_makeCalendarRange($params['startsAfterDateTime'], $params['dateRangeEndDateTime'], false);
		$calendarDates = $r->calendar['calendarDates'];
		$r->calendar['dates'] = $calendarDates;

		foreach($calendarDates as $date)
		{
			if ($date >= $params['dateRangeStartDateTime'] && $date <= $params['dateRangeEndDateTime'])
			{
				$this->_dates[$date->format('Y-m-d')] = array('date' => $date, 'events' => array());
			}
		}


		// Build up the search criteria

		$criteria = array();

		// TODO: Add all the attributes to criteria
		// TODO: Filter eventData query by dateRangeStart, dateRangeEnd, startsBefore, startsAfter, endsBefore, endsAfter

		// If fieldHandle attribute is set, find fieldId from that, and unset fieldHandle
		if (isset($params['fieldHandle']) && !empty($params['fieldHandle']))
		{
			$criteria['fieldId'] = craft()->fields->getFieldByHandle($params['fieldHandle']);
		}
		if (isset($params['fieldId']))
		{
			$criteria['fieldId'] = $params['fieldId'];
		}

		// If the calendarHandle attribute is set, find a fieldId from that, and unset calendarHandle
		if (isset($params['calendarHandle']) && !empty($params['calendarHandle']))
		{
			$criteria['calendarId'] = craft()->calendars_calendars->getCalendarByHandle($params['calendarHandle']);
		}
		if (isset($params['calendarId']))
		{
			$criteria['calendarId'] = $params['calendarId'];
		}

		if (isset($params['elementId']))
		{
			$criteria['elementId'] = $params['elementId'];
		}

		// Now go fetch all the EventData!

		$timerStartEventDataQuery = microtime();
		$r->debug['timeToEventDataQuery'] = $timerStartEventDataQuery - $timerStartProcessing;
		$eventData = $r->eventData = $this->getEventDataByCriteria($criteria);
		$r->debug['eventDataQueryTime'] = microtime() - $timerStartEventDataQuery;



		// TODO: BIGASS REFACTOR:
		// Iterate over all the eventData...
		// If the event occurence is in the calendarRange, register it.
		// Generate any event recurrences that could be within our calendarRange.
		// If the recurrence is in the calendarRange, register it.



		// For each calendar date,
		// check each event to see if its occurrences or recurrences fall inside of our calendar range

		if (!empty($calendarDates) && !empty($eventData))
		{

			// Looping over the calendar dates
			foreach ($calendarDates as $date)
			{

				// Looping through all the possible eventData
				foreach ($eventData as $event)
				{

					// Calculate the occurrences or recurrences on this date
					$occurrences = $this->_occurrenceOnDay($event, $date);
					$recurrences = $this->_recurrencesOnDay($event, $date);
					$possibleEvents = array_merge($occurrences, $recurrences);

					// Filter all the possible event instances by our criteria
					$matchingEvents = $this->_filterEventsByCriteria($possibleEvents, $params);

					// ...and add the matching events to the calendar!
					foreach ($matchingEvents as $matchingEvent)
					{
						$this->_registerEvent($matchingEvent);
						$this->_registerElementToFetch($matchingEvent);
					}

					// (Whew.)

				}

			}

		}

		// Wrap things up and return

		$r->events = $this->_events;
		$r->dates = $this->_dates;
		$r->elementsToFetch = $this->_elementsToFetch;
		$r->elements = $params['fetchElements'] ? $this->_fetchElements() : array();
		$r->debug['timeToProcessEnd'] = microtime() - $timerStartProcessing;

		return $r;

	}

	public function getMonthCalendar($params = array()) {

		// First, figure out the first day of the month of the given dateRangeStart date

		if (!isset($params['calendarDay']))
		{
			$params['calendarDay'] = "today";
		}
		$givenDateTime = $this->DT($params['calendarDay']);

		$firstDayOfMonth = (clone $givenDateTime);
		$firstDayOfMonth->setDate($firstDayOfMonth->year(), $firstDayOfMonth->month(), 1);

		// Now also calculate the first day of the next and previous months

		$nextMonthDateTime = (clone $firstDayOfMonth);
		$nextMonthDateTime->modify('+1 month');

		$prevMonthDateTime = (clone $firstDayOfMonth);
		$prevMonthDateTime->modify('-1 month');

		$calendarStartDateTime = (clone $firstDayOfMonth);

		$today = $this->DT('today');

		// Number of days in the month
		$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $firstDayOfMonth->month(), $firstDayOfMonth->year());

		// The weekday of the first day of the month
		$dayOfTheWeek = (int) $firstDayOfMonth->format("N");

		// Counter for weeks of the month
		$weekOfTheMonth = 0;

		// Day to start the week
		if (!isset($params['weekStartDay']))
		{
			$params['weekStartDay'] = 'monday';
		}
		$weekStartDay = array_search( strtolower($params['weekStartDay']), $this->_weekdays );
		if ($weekStartDay === FALSE) { $weekStartDay = 0; }

		// Calculate number of leading days (prev month)
		if ($leadingDays = (($dayOfTheWeek - $weekStartDay) + 7) % 7)
		{
			$calendarStartDateTime->modify("- {$leadingDays} days");
		}

		// Keep track of start date
		$currentDate = clone $calendarStartDateTime;

		// Initiate weeks and weekdays arrays
		$weeks = $weekdays = $days = array();

		// Initiate day count
		$day_count = 0;

		// Populate weeks array

		while (true)
		{
			// Initiate week
			if ( ! isset($weeks[$weekOfTheMonth]) )
			{
				$weeks[$weekOfTheMonth] = array(
					'days'     => array(),
				);
			}

			// Add the day row to the week
			$weeks[$weekOfTheMonth]['days'][] = array(
				'date' => clone $currentDate,
				'isNextMonth' => ($currentDate->format('Ym') == $nextMonthDateTime->format('Ym')) ? true : false,
				'isPrevMonth' => ($currentDate->format('Ym') == $prevMonthDateTime->format('Ym')) ? true : false,
				'isCurrentMonth' => ($currentDate->format('Ym') == $givenDateTime->format('Ym')) ? true : false,
				'isGivenDay' => ($currentDate->format('Ymd') == $givenDateTime->format('Ymd')) ? true : false,
				'isToday' => ($currentDate->format('Ymd') == $today->format('Ymd')) ? true : false,
			);

			// The first time through (when $weekOfTheMonth is 0), populate the weekdays array
			if ( ! $weekOfTheMonth)
			{
				$weekdays[] = array(
					'weekday' => $currentDate->format('l'),
					'weekdayShort' => $currentDate->format('D'),
					'weekdayFirst' => substr($currentDate->format('D'), 0, 1),
					'date' => $currentDate
				);
			}

			// Advance by one day
			$currentDate->modify('+1 day');

			// if days is divisible by 7, a week is done
			if ($done = !(++$day_count % 7))
			{
				// If we're caught up with the next month too, exit the loop
				if ($currentDate->format('Ym') == $nextMonthDateTime->format('Ym')) break;

				// Or else just increase the week of the month
				$weekOfTheMonth++;
			}
		}

		// End date
		$calendarEndDateTime = $currentDate;

		// Set up an object to return to the template
		$r = (Object) array(
			'params' => $params,
			'daysInMonth' => $daysInMonth,
			'givenDay' => $givenDateTime,
			'today' => $today,
			'thisMonth' => $firstDayOfMonth,
			'nextMonth' => $nextMonthDateTime,
			'prevMonth' => $prevMonthDateTime,
			'calendarStart' => $calendarStartDateTime,
			'calendarEnd' => $calendarEndDateTime,
			'weekdays' => $weekdays,
			'weeks' => $weeks,
			'eventData' => null,
		);

		if ( isset($params['includeEventData']) and $params['includeEventData'] == false )
		{
			return $r;
		}

		// Now go get the the events

		$params['dateRangeStart'] = $calendarStartDateTime;
		$dateRangeEnd = clone $calendarEndDateTime;
		$params['dateRangeEnd'] = $dateRangeEnd->modify('+1 day');

		$eventData = $this->getEventDataCriteriaModel($params);

		// Attach the events to calendar days

		$eventDataDates = $eventData->dates();

		foreach ($weeks AS &$week)
		{
			foreach ($week['days'] AS &$day)
			{
				if ( isset( $eventDataDates[ $day['date']->format('Y-m-d') ] ) )
				{
					$day['events'] = $eventDataDates[ $day['date']->format('Y-m-d') ]['events'];
				}
				else
				{
					$day['events'] = array();
				}
			}
		}

		// Having attached the events to the days inside $weeks, we reattach $weeks to the return object.
		$r->weeks = $weeks;
		// And we'll make the EventDataCriteriaModel available, too, just cuz.
		$r->eventData = $eventData;
		return $r;

	}

	private function _occurrenceOnDay(Calendars_EventDataModel $eventData, DateTime $calendarDay = null)
	{

		// If $calendarDay is not provided, assume we're working with today
		if (is_null($calendarDay))
		{
			$calendarDay = $this->DT('today');
		}

		// Make sure we're working in the local system DateTimeZone
		$eventStartDate = $this->DT($eventData->startDate);

		// If the start date matches the calendar day, we have a winner!
		if ( $calendarDay->format('Ymd') == $eventStartDate->format('Ymd') )
		{

			$attributes = $eventData->getAttributes();

			$attributes['isOriginalOccurrence'] = true;
			$attributes['isRecurrence'] = false;

			$n = new Calendars_EventDataModel($attributes);
			return array($n);

		}

		return array();

	}

	private function _recurrencesOnDay(Calendars_EventDataModel $eventData, DateTime $calendarDay = null)
	{

		// If $calendarDay is not provided, assume we're working with today
		if (is_null($calendarDay))
		{
			$calendarDay = $this->DT('today');
		}

		$recurrences = array();
		$recurrenceRules = $this->_decodeRecurrenceRules($eventData->recurrenceRules);

		foreach ($recurrenceRules as $rule)
		{
			if ($this->_ruleMatchesDay($rule, $eventData, $calendarDay))
			{

				$attributes = $eventData->getAttributes();

				$attributes['startDate'] = $this->_recurrenceStartDateTime($rule, $eventData, $calendarDay);;
				$attributes['endDate'] = $this->_recurrenceEndDateTime($rule, $eventData, $calendarDay);
				$attributes['isOriginalOccurrence'] = false;
				$attributes['isRecurrence'] = true;

				$n = new Calendars_EventDataModel($attributes);
				return array($n);

			}
		}

		return $recurrences;

	}

	// TODO: Enforce that the param is a JSON string...?
	// Brandon: Is there a JSON validator tucked away anywhere in the app core?
	private function _decodeRecurrenceRules($json)
	{

		// TODO: Actually decode the rule properly. (Add an EventDataRuleModel.)
		$rules = json_decode($json)->rules;
		foreach($rules as $i => $r)
		{
			if (!isset($r->ruleType->name)) { unset($rules[$i]); }
		}
		return $rules;

	}

	private function _recurrenceStartDateTime($rule, Calendars_EventDataModel $eventData, DateTime $calendarDay)
	{
		if (isset($rule->ruleType->name) AND $rule->ruleType->name == 'selectedDate')
		{
			return $this->DT($rule->startDate);
		}
		$originalStart = $eventData->getAttribute('startDate');
		$recurrenceStart = clone $calendarDay;
		return $recurrenceStart->setTime($originalStart->format('H'), $originalStart->format('i'));
	}

	private function _recurrenceEndDateTime($rule, Calendars_EventDataModel $eventData, DateTime $calendarDay)
	{
		if (isset($rule->ruleType->name) AND $rule->ruleType->name == 'selectedDate')
		{
			return $this->DT($rule->endDate);
		}
		$originalDuration = date_diff($eventData->getAttribute('startDate'), $eventData->getAttribute('endDate'));
		$recurrenceStart = $this->_recurrenceStartDateTime($rule, $eventData, $calendarDay);
		return $recurrenceStart->add($originalDuration);
	}

	private function _ruleMatchesDay($rule, Calendars_EventDataModel $eventData, DateTime $calendarDay)
	{

		// selectedDate rules are a special case, since they could occur on the startDate
		if ($rule->ruleType->name == 'selectedDate')
		{
			return $this->_checkSelectedDateRule($rule, $eventData, $calendarDay);
		}

		// Are we before the startDate? (An event can't recur before it occurs.)
		if (! ($calendarDay > $eventData->startDate))
		{
			return false;
		}

		// Are we past the repeatUntil date?
		if ( !$rule->repeatForever AND ($calendarDay > $this->DT($rule->repeatUntilDate)) )
		{
			return false;
		}

		// Check the rule.
		switch($rule->ruleType->name)
		{

			case 'daily':
				return $this->_checkDailyRule($rule, $eventData, $calendarDay);
				break;

			case 'weekly':
				return $this->_checkWeeklyRule($rule, $eventData, $calendarDay);
				break;

			case 'monthlyByDayOfMonth':
				return $this->_checkMonthlyByDayOfMonthRule($rule, $eventData, $calendarDay);
				break;

			case 'monthlyByDayOfWeek':
				return $this->_checkMonthlyByDayOfWeekRule($rule, $eventData, $calendarDay);
				break;

			case 'yearly':
				return $this->_checkYearlyRule($rule, $eventData, $calendarDay);
				break;

		}

		return false;

	}

	private function _filterEventsByCriteria($possibleEvents, $criteria)
	{

		$matchingEvents = array();

		foreach ($possibleEvents as $event)
		{

			// Filter out events that end before before the dateRangeStart date?
			if (isset($criteria['dateRangeStartDateTime']) && $event->endDate < $criteria['dateRangeStartDateTime'])
			{
				continue;
			}

			// Filter out events that start after the startsBefore date
			if (isset($criteria['startsBeforeDateTime']) && $event->startDate > $criteria['startsBeforeDateTime'])
			{
				continue;
			}

			// Filter out events that end after the endsBefore date
			if (isset($criteria['endsBeforeDateTime']) && $event->endDate > $criteria['endsBeforeDateTime'])
			{
				continue;
			}

			$matchingEvents[] = $event;

		}

		return $matchingEvents;

	}

	private function _registerEvent($event)
	{

		// Add the event to _events

		$this->_events[] = $event;

		// For each day in the event duration, add the event to _dates

		foreach($this->getCalendarRange($event->startDate, $event->endDate) as $date)
		{

			$dayIndex = $date->format('Y-m-d');

			if (isset($this->_dates[$dayIndex]['events']))
			{
				$this->_dates[$dayIndex]['events'][] = $event;
			}

		}

	}




	// -------- Methods for querying and registering Elements -------- //

	private function _registerElementToFetch($event)
	{

		$elementId = $event->element->id;
		$elementType = $event->elementType;

		// register the Element's ID in _elementIds
		$this->_elementIds[$elementId] = $elementId;

		// register the Element in _elementsToFetch
		if (!array_key_exists($elementType, $this->_elementsToFetch))
		{
			$this->_elementsToFetch[$elementType] = array($elementId => $elementId);
		}
		else
		{
			$this->_elementsToFetch[$elementType][$elementId] = $elementId;
		}

	}

	private function _fetchElements()
	{

		$this->_elements = array();

		foreach ($this->_elementsToFetch as $type => $ids)
		{

			$elements = craft()->elements->getCriteria($type)->find(array('id' => $ids));

			foreach ($elements as $e)
			{
				$this->_elements[$e->id] = $e;
			}

		}

		return $this->_elements;

	}




	// -------- Methods for checking eventData rules -------- //

	private function _checkSelectedDateRule($rule, Calendars_EventDataModel $eventData, DateTime $calendarDay)
	{

		$ruleStartDate = $this->DT($rule->startDate);

		// Does the date of $ruleDate match the date of $calendarDay?
		return ( $calendarDay->format('Ymd') == $ruleStartDate->format('Ymd') );

	}

	private function _checkDailyRule($rule, Calendars_EventDataModel $eventData, DateTime $calendarDay)
	{

		// Are we on the correct interval of days?
		$daysSinceOccurence = date_diff($eventData->startDate, $calendarDay)->days;
		return ($daysSinceOccurence % intval($rule->daysInterval) == 0);

	}

	private function _checkWeeklyRule($rule, Calendars_EventDataModel $eventData, DateTime $calendarDay)
	{

		// Are we on the correct day of the week?
		if ( strpos($this->_dayOfWeek($calendarDay), $rule->daysOfWeek) === false )
		{
			return false;
		}

		// Are we on the correct interval of weeks?
		$calendarDayEnd = clone $calendarDay->setTime(23,59,59); // TODO: Test this.
		$weeksSinceOccurence = (int) floor( date_diff($eventData->startDate, $calendarDayEnd)->days / 7 );
		return ($weeksSinceOccurence % intval($rule->weeksInterval) == 0);

	}

	private function _checkMonthlyByDayOfMonthRule($rule, Calendars_EventDataModel $eventData, DateTime $calendarDay)
	{

		// Are we on the correct day of the month?
		if
		(!(
			( intval($calendarDay->format('j')) == intval($rule->dayOfMonth) )
			OR
			( $rule->dayOfMonth == 'last' AND intval($calendarDay->format('j')) == intval($calendarDay->format('t')) )
		))
		{
			return false;
		}

		// Are we on the correct interval of months?
		// TODO: Figure out a better way to calculate elapsed months.
		$calendarDayEnd = clone $calendarDay->setTime(23,59,59); // TODO: Test this.
		$diff = date_diff($eventData->startDate, $calendarDayEnd);
		$monthsSinceOccurence = ($diff->y * 12) + $diff->m;
		return ($monthsSinceOccurence % intval($rule->monthsInterval) == 0);

	}

	private function _checkMonthlyByDayOfWeekRule($rule, Calendars_EventDataModel $eventData, DateTime $calendarDay)
	{

		// Are we in the correct ordinal?
		if ($rule->dayOfWeekOrdinal == 'last')
		{
			$max = intval($calendarDay->format('t'));
			$min = $max - 6;
		}
		else
		{
			$max = 7 * intval($rule->dayOfWeekOrdinal);
			$min = $max - 6;
		}
		if ( ! in_array(intval($calendarDay->format('j')), range($min,$max)) )
		{
			return false;
		}

		// Are we on the correct day of the week?
		if ( strpos($this->_dayOfWeek($calendarDay), $rule->daysOfWeek) === false )
		{
			return false;
		}

		// Are we on the correct interval of months?
		// TODO: Figure out a better way to calculate elapsed months.
		$calendarDayEnd = clone $calendarDay->setTime(23,59,59); // TODO: Test this.
		$diff = date_diff($eventData->startDate, $calendarDayEnd);
		$monthsSinceOccurence = ($diff->y * 12) + $diff->m;
		return ($monthsSinceOccurence % intval($rule->monthsInterval) == 0);

	}

	private function _checkYearlyRule($rule, Calendars_EventDataModel $eventData, DateTime $calendarDay)
	{

		// Are we on the correct month and day?
		if ( $calendarDay->format('md') != $this->DT($eventData->startDate)->format('md') )
		{
			return false;
		}

		// Are we on the correct interval of years?
		$yearsSinceOccurence = date_diff($eventData->startDate, $calendarDay)->y;
		return ($yearsSinceOccurence % intval($rule->yearsInterval) == 0);

	}

	private function _dayOfWeek(DateTime $calendarDay)
	{
		return $calendarDay->format('D');
	}




	// -------- Methods for creating localized DateTime and DateTimeZone objects -------- //

	public function DTZ()
	{
		if (!isset($this->_systemDateTimeZone))
		{
			$this->_systemDateTimeZone = new \DateTimeZone(craft()->getTimeZone());
		}
		return $this->_systemDateTimeZone;
	}

	public function DT($date)
	{

		// If I pass in a string, I want to init a DateTime corresponding to that time in the system DateTimeZone
		if (is_string($date))
		{
			return new DateTime($date, $this->DTZ());
		}

		// If I pass in a DateTime, I want to init a DateTime corresponding to that time in the system DateTimeZone
		if ($date instanceof DateTime)
		{
			return $date->setTimezone($this->DTZ());
		}

		// If I pass in a native PHP \DateTime, I want to turn it into a \Craft\DateTime object in the system DateTimeZone
		if ($date instanceof \DateTime)
		{
			$date = DateTime::createFromString($date->getTimestamp());
			return $date->setTimezone($this->DTZ());
		}

		// Otherwise (if $date is null or not a recognized type), give me today's system-local DateTime
		throw new Exception("Craft Calendars is unable to create a DateTime from an unrecognized format.");

	}




	/**
	 * LOL sample event data array
	 *
	 * @return array An event data array (with keys: events, dates, elements, elementIds, debugData)
	 */
	public function getSampleEventDataArray()
	{

		$r = array(
			'events' => array(
				'event1',
				'event2',
				'event3',
			),
			'dates' => array(
				$this->DT('+0 days'),
				$this->DT('+1 days'),
				$this->DT('+2 days'),
			),
			'elements' => array(),
			'elementIds' => array(
				1,
				2,
				3,
			),
			'debugData' => array(
				'thing1',
				'thing2'
			)
		);

		return $r;

	}


}