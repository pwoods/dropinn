<?php
namespace Craft;

/**
 * Craft Calendars BetaService
 */
class Calendars_BetaService extends BaseApplicationComponent
{

	const SecondStarToTheRight = 'http://michaelrog.com/craftcalendars/et/home.php';

	public function phoneHome() {

		$et = new Et(static::SecondStarToTheRight);
		$et->phoneHome();

	}

}