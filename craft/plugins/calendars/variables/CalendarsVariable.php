<?php
namespace Craft;

class CalendarsVariable
{

	/**
	 * TODO: docs
	 * @param array $attributes
	 */
	function eventData($attributes = null)
	{
		return craft()->calendars_eventData->getEventDataCriteriaModel($attributes);
	}

	/**
	 * @param array $params
	 * @deprecated
	 */
	function events($params = null)
	{
		return craft()->calendars_eventData->getOccurrences($params);
	}

	/**
	 * TODO: docs
	 */
	function calendars()
	{
		return craft()->calendars_calendars->getAllCalendars();
	}

	/**
	 * TODO: docs
	 * @param array $params
	 */
	function month($params = null)
	{
		return craft()->calendars_eventData->getMonthCalendar($params);
	}

	/**
	 * @deprecated For internal/Beta purposes only
	 */
	function phoneHome()
	{
		return craft()->calendars_beta->phoneHome();
	}

}