<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
	'omitScriptNameInUrls' => true,
	'siteUrl' => array(
		'en_gb' => 'http://dropinn.net/',
		'en_gb_gcc' => 'http://grace-community.church/',
	),
	'imageDriver' => 'imagick'
);