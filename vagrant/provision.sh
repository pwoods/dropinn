#!/bin/sh

# this php7 box has a problem with key permissions, this should fix it
chmod go-rwx /home/vagrant/.ssh/*

date > /etc/vagrant_provisioned_at
service mysqld restart

/usr/bin/mysql -u root -e "DROP DATABASE IF EXISTS vagrant;"
mysqladmin -u root create vagrant
/usr/bin/mysql -u root -e "GRANT ALL PRIVILEGES ON * . * TO 'vagrant'@'%' IDENTIFIED BY 'vagrant';"

cp /vagrant/vagrant/httpd-start.conf /etc/init/httpd.conf -f
cp /vagrant/vagrant/httpd.conf /etc/httpd/conf/httpd.conf -f
cp /vagrant/vagrant/php.ini /etc/php.ini -f
cp /vagrant/vagrant/xdebug.ini /etc/php.d/xdebug.ini -f
cp /vagrant/vagrant/site.config.php /vagrant/settings/site.config.php -f

/vagrant/bin/custard compile:scss /vagrant/static/scss /vagrant/static/css

/vagrant/bin/custard stem:update-schema
/vagrant/bin/custard stem:seed-data

service httpd restart
